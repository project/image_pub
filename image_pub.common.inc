<?php

/*
 * Generalized helper functions for tasks that require tight integration 
 * with other parts of Drupal, such as image.module and taxonomy.module.
 *	_image_pub_get_vid()
 *	_image_pub_base_url()
 *	_image_pub_image_baseurl()
 *	_image_pub_get_imagefilename()
 *	_image_pub_get_imageinfo()
 *	_image_pub_album_get()
 *	_image_pub_album_enum()
 *	_image_pub_album_selector()
 *	_image_pub_album_access()
 *	_image_pub_album_images()
 *
 *	_image_pub_authenticate()
 *	_image_pub_album_add()
 *	_image_pub_image_add()
 */

require_once 'image_pub.common.inc';

function _image_pub_get_vid() {
  return _image_gallery_get_vid();
}
function _image_pub_base_url() {
  global $base_url;
  return $base_url;
}
function _image_pub_image_baseurl() {
  return _image_pub_base_url() .'/'. variable_get('file_directory_path', 'files') .'/'. variable_get('image_default_path', 'images');
}
function _image_pub_get_imagefilename($node, $size = '_original') {
  $fname = $node->images[$size];
  if (isset($fname)) {
    $imgbase = variable_get('image_default_path', 'images') .'/';
    if (!strncmp($fname, $imgbase, strlen($imgbase))) {
      $fname = substr($fname, strlen($imgbase));
    }
  }
  return $fname;
}
function _image_pub_get_imageinfo($node, $size = '_original') {
  $file = file_create_path($node->images[$size]);
  $info = image_get_info($file);
  $info['filesize'] = filesize($file);
  return $info;
}
function _image_pub_album_get($albumid) {
  
  $term = taxonomy_get_term($albumid);
  if (!$term) {
  	if ($albumid == 0) {
      $term = taxonomy_get_term('<root>');
    }
    else {
      $term = NULL;
    }
  }
  return $term;
}
function _image_pub_album_enum($albumid = 0, $subalbums = TRUE) {
  return taxonomy_get_tree(_image_pub_get_vid(), $albumid, -1,
			   ($subalbums ? NULL : 1));
}
function _image_pub_album_selector($fname, $showroot = FALSE, $selalbum = NULL) {
  /* This is mostly redundant with taxonomy_form(). */
  $body = array();
  $tree = _image_pub_album_enum();
  $options = array();
  $albumid = 0;
  if (isset($selalbum)) {
    $albumid = $selalbum->tid;
  }
  if ($showroot) {
    $options[0] = '&lt;Root Album&gt;';
  }
  if ($tree) {
    foreach ($tree as $term) {
      if (!$showroot && ($albumid == 0)) {
        $albumid = $term->tid;
      }
      $dashes = '';
      for ($i = 0; $i < $term->depth; $i++) {
        $dashes .= '-';
      }
      $options[$term->tid] = $dashes . $term->name;
    }
  }
  $body[] = '<select name="' . $fname . '">';
  foreach ($options as $aid => $term) {
    $sel = '';
    if ($aid == $albumid) {
      $sel = ' selected="selected"';
    }
    $body[] = '<option value="' .$aid . '"' . $sel . '>' . $term . '</option>';
  }
  $body[] = '</select>';
  return implode("\r\n", $body);
}

function _image_pub_album_access($type, $album) {
  switch ($type) {
    case 'view':
      return user_access('access content');
    case 'update':
      return user_access('administer images');
    case 'create':
      return user_access('create images');
  }
  return FALSE;
}
function _image_pub_album_images($tid) {
  $nodes = array();
  $sql = "SELECT n.nid FROM {node} n INNER JOIN {term_node} t ON t.nid = n.nid WHERE t.tid = $tid AND n.type = 'image';";
  $res = db_query($sql);
  while ($term = db_fetch_object($res)) {
    $nodes[] = node_load(array('nid' => $term->nid));
  }
  return $nodes;
}
function _image_pub_authenticate($uname, $pass) {
  global $user;
  if ($user->uid) {
    watchdog('image_pub', 'Session closed for %user', array('%user' => theme('placeholder', $user->name)));
    unset($user);
  }
  $user = user_authenticate(array('name' => $uname, 'pass' => $pass));
  if (!$user->uid) {
    return FALSE;
  }
  watchdog('image_pub', 'Session opened for %user', array('%user' => theme('placeholder', $user->name)));
  return TRUE;
}
function _image_pub_album_add($title, $descr, $palbum) {
  watchdog('image_pub', 'Album added with title %title', array('%title' => $title));

  if (empty($title)) {
    $title = 'Untitled album';
  }
  $edit = array(
    'name' => $title,
    'description' => $descr,
    'vid' => _image_pub_get_vid(),
    'parent' => array(isset($palbum) ? $palbum->tid : 0),
    'weight' => 0,
  );
  $term = taxonomy_save_term($edit);
  if (isset($term)) {
    // taxonomy_save_term amends $edit directly, and simply returns a status
    // return (object)$edit;
    return (object)$edit;
  }
  return NULL;
}
function _image_pub_image_add($album, $caption, $description, $srcfield) {
  global $user;
  if (!isset($_FILES[$srcfield])) {
    return array('success' => FALSE,
                 'reason' => 'Forgetting a file?');
  }

  if (empty($caption)) {
    $caption = $_POST['force_filename'];
    if (empty($caption)) {
      $caption = basename($_FILES[$srcfield]['name']);
      if (empty($caption)) {
        $caption = 'Untitled image';
      }
    }
  }

  // Create a drupal node
  // In order for the image upload to work, the name, tmp_name and error 
  // of the $_FILES[$srcfield] have to be copied to locations
  // that file_check_upload() recognises.
  // The size needs to go into $_FILES['files']['size']['image'] for image
  // module's size checking to occur.
  $_FILES['files']['name']['image'] = $_FILES[$srcfield]['name'];
  $_FILES['files']['tmp_name']['image'] = $_FILES[$srcfield]['tmp_name'];
  $_FILES['files']['error']['image'] = $_FILES[$srcfield]['error'];
  $_FILES['files']['size']['image'] = filesize($_FILES[$srcfield]['tmp_name']);

  $form_state = array();
  module_load_include('inc', 'node', 'node.pages'); 
  $node = array('type' => 'image');
  $form_state['values']['title'] = $caption;
  $form_state['values']['uid'] = $user->uid;
  $form_state['values']['name'] = $user->name;
  $form_state['values']['body_field'] = $description;
  $form_state['values']['taxonomy'][_image_pub_get_vid()] = $album->tid;
  $form_state['values']['op'] = t('Save');
  $redirect_url = drupal_execute('image_node_form', $form_state, (object)$node);

  // Check that the uploaded image was accepted.
  $errors = form_get_errors();
  if (!empty($errors)) {
    return array('success' => FALSE,
                 'reason' => strip_tags(implode($errors, ' ')));
  }

  watchdog('image_pub', '%image was added.', array('%image' => $_FILES[$srcfield]['name']));
  return array('success' => TRUE, 'redirect_url' => $redirect_url);
}
