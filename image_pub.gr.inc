<?php

require_once 'image_pub.common.inc';

/*
 * Definitions for handling the gallery remote protocol
 * See http://gallery.menalto.com/ for more info.
 */

define('GR_STAT_SUCCESS', 0);
define('GR_STAT_PROTO_MAJ_VER_INVAL', 101);
define('GR_STAT_PROTO_MIN_VER_INVAL', 102);
define('GR_STAT_PROTO_VER_FMT_INVAL', 103);
define('GR_STAT_PROTO_VER_MISSING', 104);
define('GR_STAT_PASSWD_WRONG', 201);
define('GR_STAT_LOGIN_MISSING', 202);
define('GR_STAT_UNKNOWN_CMD', 301);
define('GR_STAT_NO_ADD_PERMISSION', 401);
define('GR_STAT_NO_FILENAME', 402);
define('GR_STAT_UPLOAD_PHOTO_FAIL', 403);
define('GR_STAT_NO_WRITE_PERMISSION', 404);
define('GR_STAT_NO_CREATE_ALBUM_PERMISSION', 501);
define('GR_STAT_CREATE_ALBUM_FAILED', 502);

define('GR_SERVER_VERSION', '2.15');

function _image_pub_gr_get_albumname($term) {
  return 'Album' . $term->tid;
}
function _image_pub_gr_get_albumid($albname) {
  if (!strncmp($albname, 'Album', 5)) {
    return substr($albname, 5);
  }
  return 0;
}


/*
 * Protocol request handler entry points below
 *    _image_pub_gr_request()               (main entry point)
 *    _image_pub_gr_finish()                (completion helper)
 *    _image_pub_gr_login()
 *    _image_pub_gr_fetch_albums()
 *    _image_pub_gr_fetch_album_images()
 *    _image_pub_gr_add_album()
 *    _image_pub_gr_move_album()
 *    _image_pub_gr_add_image()
 */

function _image_pub_gr_request() {
  if (isset($_POST['cmd'])) {
    $cmd = $_POST['cmd'];
  }
  else {
    $cmd = $_GET['cmd'];
  }

  $numref = FALSE;

  // watchdog('image_pub', 'Processing command %cmd', array('%cmd' => $cmd));
 
  switch ($cmd) {
    case 'login':
      _image_pub_gr_login($_POST['uname'], $_POST['password']);
      break;

    case 'fetch-albums':
      $numref = TRUE;
    case 'fetch-albums-prune':
      $check_writeable = ($_POST['check-writeable'] == 'yes') ? TRUE : FALSE;
      _image_pub_gr_fetch_albums($numref, $check_writeable);
      break;

    case 'fetch-album-images':
      $albums_too = ($_POST['albums_too'] == 'yes') ? TRUE : FALSE;
      _image_pub_gr_fetch_album_images($_POST['set_albumName'],
                                       $albums_too);
      break;

    case 'new-album':
      _image_pub_gr_add_album($_POST['set_albumName'],
                              $_POST['newAlbumTitle'],
                              $_POST['newAlbumDesc']);
      break;

    case 'move-album':
      _image_pub_gr_move_album($_POST['set_albumName'],
                               $_POST['set_destalbumName']);
      break;

    case 'add-item':
      _image_pub_gr_add_image($_POST['set_albumName'],
	      $_POST['caption'],
	      $_POST['extrafield_Description']);
      break;

    case '':
      echo 'For more information about Gallery Remote, see Gallery\'s website located at <a href="http://gallery.sourceforge.net">http://gallery.sourceforge.net</a>';
      exit;

    default:
      _image_pub_gr_finish(GR_STAT_UNKNOWN_CMD, '', t('Unknown command "%cmd"', array('%cmd' => theme('placeholder', $cmd))));
      break;
  }
}


function _image_pub_gr_finish($code, $body = NULL, $message = NULL) {
  static $gr_messages;
  if (!isset($gr_messages)) {
    $gr_messages = array(
      GR_STAT_SUCCESS => t('Successful'),
      GR_STAT_PROTO_MAJ_VER_INVAL => t('The protocol major version the client is using is not supported.'),
      GR_STAT_PROTO_MIN_VER_INVAL => t('The protocol minor version the client is using is not supported.'),
      GR_STAT_PROTO_VER_FMT_INVAL => t('The format of the protocol version string the client sent in the request is invalid.'),
      GR_STAT_PROTO_VER_MISSING => t('The request did not contain the required protocol_version key.'),
      GR_STAT_PASSWD_WRONG => t('The password and/or username the client send in the request is invalid.'),
      GR_STAT_LOGIN_MISSING => t('The client used the login command in the request but failed to include either the username or password (or both) in the request.'),
      GR_STAT_UNKNOWN_CMD => t('The value of the cmd key is not valid.'),
      GR_STAT_NO_ADD_PERMISSION => t('The user does not have permission to add an item to the gallery.'),
      GR_STAT_NO_FILENAME => t('No filename was specified.'),
      GR_STAT_UPLOAD_PHOTO_FAIL => t('The file was received, but could not be processed or added to the album.'),
      GR_STAT_NO_WRITE_PERMISSION => t('No write permission to destination album.'),
      GR_STAT_NO_CREATE_ALBUM_PERMISSION => t('A new album could not be created because the user does not have permission to do so.'),
      GR_STAT_CREATE_ALBUM_FAILED => t('A new album could not be created, for a different reason (name conflict).'),
    );
  }
  if (!isset($message)) {
    $message = $gr_messages[$code];
    if (!isset($message)) {
      $message = 'Undefined error code';
    }
  }
  if ($code != GR_STAT_SUCCESS) {
    watchdog('image_pub', 'Request failure: %code, %msg', array('%code' => $code, '%msg' => $message));
  } 
  else {
    //watchdog('image_pub', 'Command succeeded: %msg', array('%msg' => theme('placeholder', $message)));
  }
  drupal_set_header("Content-Type: text/plain");
  echo "#__GR2PROTO__\n" .
    $body .
    "status=$code\n" .
    "status_text=$message\n";
  exit;
}


function _image_pub_gr_login($uname, $pass) {
  global $user;
  $body = 'server_version=' . GR_SERVER_VERSION . "\n";
  if (!$uname || !$pass) {
    if (!$uname && !$pass) {
      // Not trying to log in, just asking for a version number.
      _image_pub_gr_finish(GR_STAT_SUCCESS, $body);
    }
    _image_pub_gr_finish(GR_STAT_LOGIN_MISSING);
  }
  if (!_image_pub_authenticate($uname, $pass)) {
    _image_pub_gr_finish(GR_STAT_PASSWD_WRONG);
  } 
  else {
    _image_pub_gr_finish(GR_STAT_SUCCESS, $body);
  }
}


function _image_pub_gr_fetch_albums($refnum, $check_writeable) {
  $body = '';
  $thumb_dims = image_get_sizes(IMAGE_THUMBNAIL);
  $thumb_max = min($thumb_dims['width'], $thumb_dims['height']);
  $preview_dims = image_get_sizes(IMAGE_PREVIEW);
  $preview_max = min($preview_dims['width'], $preview_dims['height']);
  $nalbums = 0;
  $cperm = 'false';
  $aperm = 'false';
  $crperm = 'no';
  if (user_access('create images')) {
    $cperm = 'true';
  }
  if (user_access('administer images')) {
    $aperm = 'true';
    $crperm = 'yes';
  }
  $tree = _image_pub_album_enum();
  $aid = 0;
  $aidref = array();
  foreach ($tree as $term) {
    if (_image_pub_album_access('view', $term)) {
      $aid++;
      $aname = _image_pub_gr_get_albumname($term);
      $aidref[$aname] = $aid;

      $pname = 0;
      if (!empty($term->parents[0])) {
        $pterm = _image_pub_album_get($term->parents[0]);
        if ($refnum) {
          $pname = $aidref[_image_pub_gr_get_albumname($pterm)];   // Guaranteed to exist?
        } 
        else {
          $pname = _image_pub_gr_get_albumname($pterm);
        }
      }

      $body .= 'album.name.' .             $aid . '=' . $aname . "\n";
      $body .= 'album.title.' .            $aid . '=' . $term->name . "\n";
      $body .= 'album.summary.' .          $aid . '=' . $term->description . "\n";
      $body .= 'album.parent.' .           $aid . '=' . $pname . "\n";
      $body .= 'album.resize_size.' .      $aid . '=' . $preview_max . "\n";
      $body .= 'album.max_size.' .         $aid . '=' . '0' . "\n";
      $body .= 'album.thumb_size.' .       $aid . '=' . $thumb_max . "\n";
      $body .= 'album.perms.add.' .        $aid . '=' . $cperm . "\n";
      $body .= 'album.perms.write.' .      $aid . '=' . $aperm . "\n";
      $body .= 'album.perms.del_item.' .   $aid . '=' . $aperm . "\n";
      $body .= 'album.perms.del_alb.' .    $aid . '=' . $aperm . "\n";
      $body .= 'album.perms.create_sub.' . $aid . '=' . $aperm . "\n";
      $body .= 'album.extrafields.' .      $aid . "=Description\n";
      $nalbums++;
    }
  }
  $body .= 'album_count=' .     $nalbums . "\n";
  $body .= 'can_create_root=' . $crperm . "\n";
  _image_pub_gr_finish(GR_STAT_SUCCESS, $body);
}


function _image_pub_gr_fetch_album_images($albname, $albumstoo) {
  $body = '';
  $album = _image_pub_album_get(_image_pub_gr_get_albumid($albname));
  if (!isset($album) || !_image_pub_album_access('view', $album)) {
    _image_pub_gr_finish(GR_STAT_NO_FILENAME, $body, 'No such album');
  }

  if ($albumstoo) {
    $tree = _image_pub_album_enum($album->tid, FALSE);
    foreach ($tree as $term) {
      if (_image_pub_album_access('view', $term)) {
        $body .= 'album.name.' . $term->tid . '=' . _image_pub_gr_get_albumname($term) . "\n";
      }
    }
    $numimages = taxonomy_term_count_nodes($album->tid, 'image');

  } 
  else {
    $numimages = 0;
    $nodes = _image_pub_album_images($album->tid);
    $iid = 0;
    foreach ($nodes as $node) {
      if (node_access('view', $node)) {
        $iid++;
        $body .= 'image.name.' .                   $iid . '=' . _image_pub_get_imagefilename($node) . "\n";
        $info = _image_pub_get_imageinfo($node);
        $body .= 'image.raw_width.' .              $iid . '=' . $info['width'] . "\n";
        $body .= 'image.raw_height.'.              $iid . '=' . $info['height'] . "\n";
        $body .= 'image.raw_filesize.' .           $iid . '=' . $info['filesize'] . "\n";

        $body .= 'image.resizedName.' .            $iid . '=' . _image_pub_get_imagefilename($node, IMAGE_PREVIEW) . "\n";
        $info = _image_pub_get_imageinfo($node, IMAGE_PREVIEW);
        $body .= 'image.resized_width.' .          $iid . '=' . $info['width'] . "\n";
        $body .= 'image.resized_height.' .         $iid . '=' . $info['height'] . "\n";
        //$body .= 'image.resized_filesize.'.$iid.'='.$info['filesize'] . "\n";

        $body .= 'image.thumbName.' .              $iid . '=' . _image_pub_get_imagefilename($node, IMAGE_THUMBNAIL) . "\n";
        $info = _image_pub_get_imageinfo($node, IMAGE_THUMBNAIL);
        $body .= 'image.thumb_width.' .            $iid . '=' . $info['width'] . "\n";
        $body .= 'image.thumb_height.' .           $iid . '=' . $info['height'] . "\n";
        //$body .= 'image.thumb_filesize.' . $iid . '=' . $info['filesize'] . "\n";

        $body .= 'image.caption.' .                $iid . '=' . $node->title . "\n";
        $body .= 'image.clicks.' .                 $iid . '=' . '0' . "\n";
        $body .= 'image.extrafield.Description.' . $iid . '=' .$node->teaser . "\n";

        $ctime = $node->created;
        $body .= 'image.capturedate.year.' .       $iid . '=' . date('Y', $ctime) . "\n";
        $body .= 'image.capturedate.mon.' .        $iid . '=' . date('n', $ctime) . "\n";
        $body .= 'image.capturedate.mday.' .       $iid . '=' . date('j', $ctime) . "\n";
        $body .= 'image.capturedate.hours.' .      $iid . '=' . date('H', $ctime) . "\n";
        $body .= 'image.capturedate.minutes.' .    $iid . '=' . date('i', $ctime) . "\n";
        $body .= 'image.capturedate.seconds.' .    $iid . '=' . date('s', $ctime) . "\n";
        $body .= 'image.hidden.' .                 $iid . '=' . ($node->status != 1 ? 'yes' : 'no') . "\n";
        $numimages++;
      }
    }
  }

  $body .= 'image_count=' . $numimages . "\n";
  $body .= 'baseurl=' . _image_pub_image_baseurl() . "/\n";

  _image_pub_gr_finish(GR_STAT_SUCCESS, $body);
}


function _image_pub_gr_add_album($parentaname, $title, $descr) {
  $body = '';

  $palbum = _image_pub_album_get(_image_pub_gr_get_albumid($parentaname));
  if (!isset($palbum)) {
    _image_pub_gr_finish(GR_STAT_CREATE_ALBUM_FAILED, $body, t('No such parent album: %parent', array('%parent' => $parentaname)));
  }
  if (!_image_pub_album_access('update', $palbum)) {
    _image_pub_gr_finish(GR_STAT_NO_CREATE_ALBUM_PERMISSION, $body);
  }

  $term =_image_pub_album_add($title, $descr, $palbum);
  if (!isset($term)) {
    _image_pub_gr_finish(GR_STAT_CREATE_ALBUM_FAILED);
  } 
  else {
    $body .= 'album_name=' . _image_pub_gr_get_albumname($term) . "\n";
    _image_pub_gr_finish(GR_STAT_SUCCESS, $body, 'Album created');
  }
}


function _image_pub_gr_move_album($albname, $destaname) {
  $body = '';

  $album = _image_pub_album_get(_image_pub_gr_get_albumid($albname));
  if (!isset($album)) {
    _image_pub_gr_finish(GR_STAT_CREATE_ALBUM_FAILED, $body, 'No such album');
  }

  if (!_image_pub_album_access('update', $album)) {
    _image_pub_gr_finish(GR_STAT_NO_WRITE_PERMISSION, $body);
  }

  if (!isset($destaname) || ($destaname == '0') || ($destaname == 'rootalbum')) {
    $dtid = 0;
  } 
  else {
    $dalbum = _image_pub_album_get(_image_pub_gr_get_albumid($destaname));
    if (!isset($dalbum) || !_image_pub_album_access('update', $dalbum)) {
      _image_pub_gr_finish(GR_STAT_CREATE_ALBUM_FAILED, $body, 'Invalid destination album '.$destaname);
    }
    $dtid = $dalbum->tid;
  }

  $album->parent = array($dtid);
  $term = (array)$album;
  $status = taxonomy_save_term($term);

  _image_pub_gr_finish(GR_STAT_SUCCESS, $body, 'Album reparented');
}


function _image_pub_gr_add_image($albname, $caption, $description) {
  $body = '';
  $album = _image_pub_album_get(_image_pub_gr_get_albumid($albname));
  if (!isset($album)) {
    _image_pub_gr_finish(GR_STAT_UPLOAD_PHOTO_FAIL, 'No such album');
  }
  if (!_image_pub_album_access('create', $album)) {
    _image_pub_gr_finish(GR_STAT_NO_ADD_PERMISSION);
  }

  $result = _image_pub_image_add($album, $caption, $description, 'userfile');

  if (!$result['success']) {
    _image_pub_gr_finish(GR_STAT_UPLOAD_PHOTO_FAIL, $body, t('The file was received, but could not be processed or added to the album.') . ' ' . $result['reason']);
  }
  else {
    _image_pub_gr_finish(GR_STAT_SUCCESS, $body, 'Added node '.$result['node']->nid);
  }
}
