<?php

/*
 * Windows Web publishing wizard support
 *
 * Below is support for the Windows XP Web publishing wizard.  It's just a 
 * glorified file picker and MSIE instance in a wizard form factor, but 
 * it gets the job done, and did we mention it comes with every copy of 
 * Windows?
 *
 * Microsoft even appears to have some sort of documentation for the 
 * publishing wizard on the MSDN site, although debugging is a major 
 * pain:
 *
 * http://msdn.microsoft.com/library/default.asp?url=/library/en-us/shellcc/platform/shell/programmersguide/shell_basics/shell_basics_extending/publishing_wizard/pubwiz.asp
 *
 * Our part of the wizard has three steps:
 * 1. Authenticate.
 * 2. Select or create album.
 * 3. Publish.  We send a blank page with a magic script to configure
 *    the wizard.
 *
 * We send bare HTML and don't use themes.  Why not themes?
 * -> The XP publishing wizard has a microscopic embedded MSIE instance 
 *    that we don't want to overflow with content.  That is hard to
 *    guarantee using an arbitrary theme.
 * -> Themes will display distracting links in headers and sidebars.  We 
 *    don't want the user navigating to one of the distractions.
 */

require_once 'image_pub.common.inc';

/*
 * Concoct a magic registry file to add this site to the list of
 * usable destinations for the publishing wizard.
 */
function _image_pub_xp_reghack() {
  $pub_url = url('publish_xp', array('absolute' => TRUE));
  $site_name = variable_get('site_name', 'drupal');
  $site_descr = sprintf(t("Publish Your Photos to %s."), $site_name);
  if (!strncmp($pub_url, 'https:', 6)) {
    $site_descr .= ' (Secure)';
  }
  else {
    $site_descr .= ' (Insecure)';
  }
  drupal_set_header("Cache-control: private");
  drupal_set_header("Content-Type: application/octet-stream");
  drupal_set_header("Content-Disposition: filename=install_registry.reg");

  $lines[] = 'Windows Registry Editor Version 5.00';
  $lines[] = '';
  $lines[] = '[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\PublishingWizard\PublishingWizard\Providers\\' . $site_name . ']';
  $lines[] = '"displayname"="' . $site_name . '"';
  $lines[] = '"description"="' . $site_descr . '"';
  $lines[] = '"href"="' . $pub_url . '"';
  $lines[] = '"icon"="' . _image_pub_base_url() . '/favicon.ico"';
  $lines[] = '';
  echo join("\r\n", $lines);
  exit;
}


/*
 * Implementation of wizard forms and such:
 *	_image_pub_xp_request()		(main entry point)
 *	_image_pub_xp_login()
 *	_image_pub_xp_album()
 *	_image_pub_xp_addimage()
 *	_image_pub_xp_page()		(HTML form template)
 *	_image_pub_xp_login_form()
 *	_image_pub_xp_album_form()
 *	_image_pub_xp_addimage_script()
 */

function _image_pub_xp_request() {

  /*
   * We require the user to be logged in to publish.
   * We honor cookies saved from other MSIE sessions, and don't
   * require the user to log in if they already have a session.
   */

  global $user;
  $cmd = $_REQUEST['cmd'];
  if (!$user->uid) {
    if (!isset($cmd)) {
      $cmd = 'login';
    }
  }

  switch ($cmd) {
    case 'login':
      _image_pub_xp_login($_REQUEST['uname'], $_REQUEST['password']);
      break;
    case 'addimage':
      _image_pub_xp_addimage($_REQUEST['albumid']);
    default:
      _image_pub_xp_album($_REQUEST['edit']);
      break;
  }
}


function _image_pub_xp_login($uname, $pass) {
  if (empty($uname) && empty($pass)) {
    _image_pub_xp_login_form();
  } 
  elseif (!_image_pub_authenticate($uname, $pass)) {
    drupal_set_message('Login failed. Please try again.', 'error');
    _image_pub_xp_login_form("Invalid Login", $uname);
  } 
  else {
    _image_pub_xp_album_form();
  }
}


function _image_pub_xp_album($edit) {
  if (!isset($edit)) {
    _image_pub_xp_album_form();
    return;
  }

  $create_new = $edit['create_new'];

  if ($create_new) {
    $albumid = $edit['create_parentid'];
    $create_name = $edit['create_name'];
    $create_descr = $edit['create_descr'];

    $album = _image_pub_album_get($albumid);
    if (!_image_pub_album_access('update', $album)) {
      drupal_set_message('Created album permission denied.', 'error');
      _image_pub_xp_album_form('Create Album Permission Denied', $album);
    }

    $newalbum = _image_pub_album_add($create_name, $create_descr, $album);

    if (isset($newalbum)) {
      drupal_set_message('New album '.'<em>'.$newalbum->name.'</em>'.' created.');
      _image_pub_xp_album_form('Created Album "'.$newalbum->name.'"', $newalbum);
    } 
    else {
      drupal_set_message('Error creating album '.'<em>'.$newalbum->name.'</em>.', 'error');
      _image_pub_xp_album_form('Error Creating Album', $album);
    }
    return;
  }

  // if we got here we are choosing an existing album
  $albumid = $edit['albumid'];
  $album = _image_pub_album_get($albumid);

  if (!isset($album)) {
    drupal_set_mssage('Invalid album specified.', 'error');
    _image_pub_xp_album_form('Invalid album specified');
    return;
  }
  if (!_image_pub_album_access('create', $album)) {
    drupal_set_message('Create image permission denied.', 'error');
    _image_pub_xp_album_form('Create Image Permission Denied', $album);
    return;
  }
  _image_pub_xp_addimage_script($album);
}


function _image_pub_xp_addimage($albumid) {
  $album = _image_pub_album_get($albumid);
  if (!isset($album)) {
    _image_pub_xp_album_form('Invalid album specified');
    return;
  }
  if (!_image_pub_album_access('update', $album)) {
    _image_pub_xp_album_form('Create Image Permission Denied', $album);
    return;
  }

  $result = _image_pub_image_add($album, NULL, NULL, 'userfile');
}


function _image_pub_xp_page($body, $header, $next, $back = NULL) {
  global $language;
  $site_name = variable_get('site_name', 'drupal');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">
<head>
</head>
<body>
<?php echo $body; ?>
<script>
function OnBack() {
<?php if (isset($back)) { ?>
	window.location.href = "<?php echo $back; ?>";
<?php } 
else { ?>
	window.external.FinalBack();
<?php } ?>
	window.external.SetWizardButtons(true,true,false);
}
function OnNext() {
	<?php echo $next; ?>.submit();
}
function window.onload() {
	window.external.SetHeaderText("<?php echo $site_name; ?>", "<?php echo $header; ?>");
	window.external.SetWizardButtons(true,true,false);
}
</script>
</body>
</html>
<?php
  exit;
}


function _image_pub_xp_login_form($message = NULL, $uname = NULL) {

  if (!isset($uname)) {
    $uname = '';
  }

  // Build a form array
  $form = array();

  $form['login'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
  );

  $form['login']['uname'] = array(
    '#type' => 'textfield',
    '#title' => t('User name'),
    '#default_value' => check_plain($uname),
    '#description' => t('Enter your user name.'),
  );

  $form['login']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('Enter your password.'),
  );

  $form['cmd'] = array(
    '#type' => 'hidden',
    '#value' => 'login',
  );

  $form['lcid'] = array(
    '#type' => 'hidden',
    '#value' => check_plain($_REQUEST['lcid']),
  );

  $form['langid'] = array(
    '#type' => 'hidden',
    '#value' => check_plain($_REQUEST['langid']),
  );

  $form[session_name()] = array(
    '#type' => 'hidden',
    '#value' => check_plain(session_id()),
  );

  // Build the HTML output
  $body = array();

  // Title
  $body[] = '<h1>Log in</h1>';

  // Display error message if one is active
  $body[] = theme('status_messages');

  // Render the form elements to themed HTML
  $body[] = '<form method="post" id="login">';
  $body[] = drupal_render(form_builder('login', $form));
  $body[] = '</form>';
  $body[] = '';

  // Output the wizard page
  _image_pub_xp_page(implode("\r\n", $body), 'Upload photos...', 'login');

}


function _image_pub_xp_album_form($message = NULL, $select_album = NULL) {

  /**
   * This form is currently not very elegant in how it is build
   * as it has to over-ride #parents to be compatible with the rest
   * of the existing code. Field sets are used to present the data
   * in logical blocks. It may be better in the longer term to make an
   * additional form page - decide it you want an existing or a new album
   * and then take the user there, rather than doing it all in one.
   * Leave as is for now just to check the concept is working
   */

  // Build a form array
  $form = array();
  $form_state = array();

  // create a fieldset to wrap the form in
  // note #parents must be forced for this set to work with original code
  $form['existing'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
  );

  // radio button for existing album
  $form['existing']['create_new'] = array(
    '#type' => 'radio',
    '#return_value' => 0,
    '#title' => t('Use an existing album'),
    '#attributes' => array('checked' => 'checked'),
    '#parents' => array('edit', 'create_new'),
  );

  // for some reason if a new album it doesn't always end up in the select list
  // I tried using cache_clear_all but that doesn't help
  // Leave as a bug for now

  // generate the selector by calling _taxonomy_term_select
  $form['existing']['albumid'] = _taxonomy_term_select('', '', $select_album->tid, _image_pub_get_vid(), '', FALSE, FALSE);
  $form['existing']['albumid']['#parents'] = array('edit', 'albumid');

  // reset weighting, and delete title from selector
  unset($form['existing']['albumid']['#weight']);
  unset($form['existing']['albumid']['#title']);
  unset($form['existing']['albumid']['#description']);

  // create a container for new album fields
  // call it edit to avoid having to force #parents
  $form['edit'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
  );

  // radio button for new album
  $form['edit']['create_new'] = array(
    '#type' => 'radio',
    '#return_value' => 1,
    '#title' => t('Create a new album'),
  );

  // get the parent selector by calling _image_gallery_parent_select
  $form['edit']['create_parentid'] = _image_gallery_parent_select(NULL, 'Within');

  // reset #required option in this instance
  unset($form['edit']['create_parentid']['#required']);

  $form['edit']['create_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Album name'),
  );

  $form['edit']['create_descr'] = array(
    '#type' => 'textfield',
    '#title' => t('Album description'),
  );

  // Common section - render to all forms
  $form['cmd'] = array(
    '#type' => 'hidden',
    '#value' => 'album',
  );

  $form['lcid'] = array(
    '#type' => 'hidden',
    '#value' => check_plain($_REQUEST['lcid']),
  );

  $form['langid'] = array(
    '#type' => 'hidden',
    '#value' => check_plain($_REQUEST['langid']),
  );

  $form[session_name()] = array(
    '#type' => 'hidden',
    '#value' => check_plain(session_id()),
  );

  // Build the HTML output
  $body = array();

  // Title
  $body[] = '<h2>Select an album</h2>';

  // Display error message if one is active
  $body[] = theme('status_messages');

  // Render the form elements to themed HTML
  $body[] = '<form method="post" id="album">';
  $body[] = drupal_render(form_builder('album', $form, $form_state));
  $body[] = '</form>';
  $body[] = '';

   _image_pub_xp_page(implode("\r\n", $body), 'Upload photos...', 'album');
}


/*
 * Concoct a magic ecmascript to configure the XP publishing wizard
 * to perform the required image submission tasks.
 */
function _image_pub_xp_addimage_script($album) {
  $albumid = $album->tid;
?>
<html>
<head>
<script>
function OnBack() {
}
function OnNext() {
}
function window.onload() {
	window.external.SetWizardButtons(true,true,true);
}

var xml = window.external.Property("TransferManifest");
var files = xml.selectNodes("transfermanifest/filelist/file");

for (i = 0; i < files.length; i++) {
	var postTag = xml.createNode(1, "post", "");
	postTag.setAttribute("href", "<?php echo url('publish_xp',  array('absolute' => TRUE)); ?>");
	postTag.setAttribute("name", "userfile");

	var dataTag = xml.createNode(1, "formdata", "");
	dataTag.setAttribute("name", "albumid");
	dataTag.text = "<?php echo $albumid; ?>";
	postTag.appendChild(dataTag);

	dataTag = xml.createNode(1, "formdata", "");
	dataTag.setAttribute("name", "cmd");
	dataTag.text = "addimage";
	postTag.appendChild(dataTag);

	dataTag = xml.createNode(1, "formdata", "");
	dataTag.setAttribute("name", "<?php echo session_name(); ?>");
	dataTag.text = "<?php echo session_id(); ?>";
	postTag.appendChild(dataTag);

	dataTag = xml.createNode(1, "formdata", "");
	dataTag.setAttribute("name", "userfile_name");
	dataTag.text = files[i].getAttribute("destination");
	postTag.appendChild(dataTag);

	dataTag.setAttribute("name", "action");
	dataTag.text = "SAVE";
	postTag.appendChild(dataTag);

	files.item(i).appendChild(postTag);
}

var uploadTag = xml.createNode(1, "uploadinfo", "");
var htmluiTag = xml.createNode(1, "htmlui", "");
htmluiTag.text = "<?php echo url('image/tid/'.$albumid, array('absolute' => TRUE)); ?>";
uploadTag.appendChild(htmluiTag);

xml.documentElement.appendChild(uploadTag);

window.external.Property("TransferManifest")=xml;
window.external.SetWizardButtons(true,true,true);
window.external.FinalNext();

</script>
</head>
</html>
<?php
  exit;
}
