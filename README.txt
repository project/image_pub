This module implements back-end support for 3rd party image publishing 
applications, using Drupal's image.module image infrastructure.  It 
currently supports two families of publishing software.


- Gallery Remote

To use a Gallery Remote client, configure the client with the URL of your 
Drupal root directory.  With luck, it will attempt to access the 
gallery_remote2.php entry point, for which this module registers a virtual 
handler.

This module has been tested successfully with the Java Gallery Remote 
clients, and digiKam.

To use Gallery Remote without Clean URLS enabled, place the following code into
a gallery_remote2.php file within your Drupal root directory:

<?php
#   header("Location: index.php?q=gallery_remote2.php");
$_POST['q'] = $_REQUEST['q'] = $_GET['q'] = 'gallery_remote2.php';
include('index.php');


- Windows Web Publishing Wizard

Windows XP and on have an interesting built-in shell feature called the 
Publishing Wizard.  The sidebar task "Publish this file/folder to the Web" 
invokes the Web Publishing Wizard.  When this task is invoked, the Web 
Publishing Wizard offers a list of sites to which your images can be 
published.  The list comes from the registry, and with this module, you 
can add Drupal sites to that list.

To add your site to the list of web publishing providers, enable this 
module, enter the "image publishing" administration page, and follow the
"Install Windows Web Publishing Wizard Support" link.  Allow regedit to 
merge the file with your local registry.
